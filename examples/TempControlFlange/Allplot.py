"""
Plot results from csv file.
"""
#==============================================================================*
# Imports
import pandas as pd
import matplotlib.pyplot as plt

#==============================================================================*

# Read the data
T = pd.read_csv("controlledTemperature.csv")

T["diff"] = abs(T["model.root.system1.Tin"] - T["model.root.system1.ramp.y"])

print(f"Max Tdiff = {T['diff'].max()} K")

# Plots
figT, axT = plt.subplots()
figTdiff, axTdiff = plt.subplots()
figQ, axQ = plt.subplots()

axT.plot(T["time"], T["model.root.system1.Tin"], label="Tin")
axT.plot(T["time"], T["model.root.system1.ramp.y"], label="Target")
axT.set_ylabel("Temperature [K]")

axTdiff.plot(T["time"], T["diff"], label="Tin - ramp")
axTdiff.set_ylabel("Temperature difference [K]")

axQ.plot(T["time"], T["model.root.system1.Qout"], label="Qout")
axQ.set_ylabel("Power [W]")

# Common features
for fig, ax in zip([figT, figTdiff, figQ], [axT, axTdiff, axQ]):
    ax.set_xlabel("Time [s]")
    ax.legend()
    fig.tight_layout()

figT.savefig("results/T.png")
figTdiff.savefig("results/Tdiff.png")
figQ.savefig("results/Q.png")

# plt.show()

#==============================================================================*
