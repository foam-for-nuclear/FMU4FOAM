# FMU4FOAM


## Installation

### Dependencies

```bash
# Light-weight C++ XML processing library
sudo apt-get install libpugixml-dev

# Multi-format archive and compression library
sudo apt-get install libarchive-dev 
```

### Build

```bash
# Create a Software directory (not mandatory)
cd ~/
mkdir Software
cd Software

# Clone the repo
git clone https://gitlab.com/foam-for-nuclear/FMU4FOAM.git

# Clone the submodules recursivly
git submodule update --init --recursive

# Init submodule ECI4FOAM and build
# Depending on your system, you might install additional packages (see https://gitlab.com/foam-for-nuclear/ECI4FOAM for more details)
./build-ECI4FOAM.sh -j4

# In ~/.bashrc
export LIB_ECI4FOAM=$HOME/path/to/ECI4FOAM

./Allwmake -j4

# In ~/.bashrc
export LIB_FMU4FOAM=$HOME/path/to/FMU4FOAM
```

Optional to test the correct build of the project.
```bash
pytest
```

### Example

```bash
python3 -m pip install OMSimulator

cd examples/TempControlFlange/

./Allrun
```
More details in [README.md](./examples/TempControlFlange/README.md)


--------------------------------------------------------------------------------
# Legacy

> A framework that enables the coupling of OpenFOAM with FMUs or the distribution of OpenFOAM as an FMU


Accurate CFD simulations often require complex boundary conditions that may be depend on numerous other engineering disciplines. Currently, these boundary conditions would require the addition of new models to the open-source CFD framework OpenFOAM which is error-prone and time-consuming. FMI standard offers a container (FMUs) to exchange multi-physics models that can be used to simulate these multi-physics. This library provides the possibility of coupled FMUs with OpenFOAM or exporting OpenFOAM as an FMU.

### Documentation


[Documentation](https://DLR-RY.github.io/FMU4FOAM/) hosted in github pages


### How to install?

Assumes that the [OpenFOAM version 2212](https://www.openfoam.com/download/release-history) or higher and python v3.8+ is already installed
Note: Older version are supported on v1.0.1
1. Install `FMU4FOAM` package:

requires conan version 1
```bash
pip install conan==1.58.0
```
### Compile OpenFOAM

```bash
./build-ECI4FOAM.sh # init submodule
./Allwmake
```

more details are found in the [documentation](https://DLR-RY.github.io/FMU4FOAM/)

### run example


```bash
 pip install OMSimulator 
cd examples/TempControlFlange/
./Allrun
```

