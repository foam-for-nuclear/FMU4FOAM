# Temperature Control Flange

## System

![](./results/ControlledTemperatureCoupled.png)

## How to run

```bash
./Allclean
./Allrun
# or
./Allrun_parallel

python3 Allplot.py
```

## Gallery

![](./results/Q.png)

*Fig 2. Power variation.*

![](./results/T.png)

*Fig 3. Temperature following command.*

![](./results/Tdiff.png)

*Fig 4. Temperature difference between Tin and command evolution.*
